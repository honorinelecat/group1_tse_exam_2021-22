####################### SIMULATE BOUNDS DATA #######################
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

## create random function
def Rand(start, end, num):
    res = []

    for j in range(num):
        res.append(np.random.randint(start, end))

    return res

## Short Bonds data
ShortBonds=pd.DataFrame({"beggining_date":pd.date_range( start="2010-01-01",end="2011-12-31")})
np.random.seed(365)
ShortBonds['amount'] = Rand(250,450,len(ShortBonds))
ShortBonds['price']= Rand(20,100,len(ShortBonds))
ShortBonds['InterestRate'] = [2.5]*len(ShortBonds)
ShortBonds['term']= Rand(2,5,len(ShortBonds))



## Long bounds data
LongBonds=pd.DataFrame({"beggining_date":pd.date_range( start="2010-01-01",end="2011-12-31")})
np.random.seed(365)
LongBonds['amount'] = Rand(1000,1500,len(LongBonds))
LongBonds['price']= Rand(10,80,len(LongBonds))
LongBonds['InterestRate'] = [5]*len(LongBonds)
LongBonds['term']= Rand(5,10,len(LongBonds))


## extract data from python to result folder
ShortBonds.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/ShortBonds.csv')
LongBonds.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/LongBonds.csv')


####################### SIMULATE COMPOUNDED INTREST #######################

## compounded interest for short bonds
def short_bonds(t):
    Amount = 250 * (pow((1 + 2.5 / 100), t))
    return (Amount)

## compounded interest for long bonds
def long_bonds(t):
    Amount = 1000 * (pow((1 + 5 / 100), t)) #pow is used because we want to put "t" at the exponent of "1 + 5 / 100"
    return(Amount)

## plot investment evolution and extract the plot
t=np.arange(0,50)
line, = plt.plot(t,short_bonds(t), label="Short bonds")
line, = plt.plot(t,long_bonds(t), label="Long bonds")
plt.xlabel("Time (year)")
plt.ylabel("Amount")
plt.title("Evolution of the investment")
plt.legend()
plt.show()

plt.savefig('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/Evolution_bondsINV.png')

