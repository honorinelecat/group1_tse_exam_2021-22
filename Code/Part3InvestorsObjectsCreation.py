from Part3InvestorsClassDescription import Investor, Defensive, Agressive, Mixed
import pandas as pd
#Objects' creation for the sublass "Defensive investor"
#Simulation of 200 defensive investors

deff=[]
for i in range(200):
    deff.append(Defensive(10000).ptf)

print(deff)

#Conversion of the list "deff" to a dataframe in order to convert it into a CSV

deff=pd.DataFrame(deff)
deff.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/deff.csv')



#Objects' creation for the subclass "Agressive investor"
#Simulation of 200 agressive investors

agg=[]
for i in range(200):
    agg.append(Agressive(10000).ptf)

print(agg)

#Conversion of the list "agg" to a dataframe in order to convert it into a CSV

agg=pd.DataFrame(agg)
agg.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/agg.csv')

#Objects' creation for the sublass "Mixed investor"
#Simulation of 200 mixed investors

mixed=[]
mixed1 =[]
for i in range(200):
    mixed.append(Mixed(10000).bptf)
    mixed1.append(Mixed(10000).sptf)

print(mixed)
print(mixed1)



#Conclusion: From our simulations, we clearly notice that the Agressive investors invest more in stocks than the Defensive investors invest in short term and
#long term bonds. Hence, this means that the condition suh that an investor keeps buying assets (keeps investing) is more restrictive for
#the investor's type who takes less risks, i.e the defensive investor, than for the one with the least risk aversion, i.e the agressive investor.
