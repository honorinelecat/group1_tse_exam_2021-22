import matplotlib.pyplot as plt
import pandas_datareader as web

# extract data from yahoo
tickers = ['FDX','GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']
stock = web.DataReader(tickers, data_source = 'yahoo', start = '2016-01-09', end = '2021-01-01')

# export data
stock.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stock.csv')

# compute daily return using and plot return accross the period
daily_return=stock['High'].pct_change()
daily_return.plot(figsize=(12,8))
plt.title("Returns between 2016 and 2021", fontsize=16)
plt.ylabel('Percent', fontsize=14)
plt.xlabel('Time', fontsize=14)
plt.savefig('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/daily_return.png')

# compute stock price using 'high' over the period
stock['High'].plot(figsize=(12,8))
plt.title(" Stock prices between 2016 and 2021", fontsize=16)
plt.ylabel('Percent', fontsize=14)
plt.xlabel('Time', fontsize=14)
plt.savefig('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stock_price.png')