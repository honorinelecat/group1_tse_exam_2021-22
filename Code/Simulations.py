
#Switch 25 stocks - 75 bonds

import pandas as pd
import numpy as np

ShortBonds=pd.read_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/ShortBonds.csv')
LongBonds=pd.read_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/LongBonds.csv')
stockk=pd.read_excel('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/stockk.xls')

# export data
stockk.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stockk.csv')


#Creation of the subclass "Defensive investor"

def bondportfolio(budge):
            data = pd.DataFrame({'ShortBonds': [0], 'LongBonds': [0]})
            while budge > ShortBonds['amount'].min():
                    short = np.random.choice(ShortBonds['amount'])
                    long = np.random.choice(LongBonds['amount'])
                    data = data.append({'ShortBonds': short, 'LongBonds': long }, ignore_index=True)
                    budge= budge - short -long
            return data

class Investor(object):
    def __init__(self, budget):
        self.budget=budget



class Defensive(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.ptf=bondportfolio(budget)





#Creation of the subclass "Agressive investor"


def stockportfolio(budg):
    data = pd.DataFrame({'stocks': [0]})
    while budg > 100:
        a = np.random.choice(stockk['High'])
        data = data.append({'stocks': a}, ignore_index=True)
        budg = budg - a
    return data


class Agressive(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.ptf=stockportfolio(budget)


#Creation of the subclass "Mixed investor"

class      Mixed(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.bptf=bondportfolio(budget*0.75)
        self.sptf=stockportfolio(budget*0.25)


#Objects' creation for the sublass "Mixed investor"

mixed2=[]
mixed3 =[]
for i in range(200):
    mixed2.append(Mixed(10000).bptf)
    mixed3.append(Mixed(10000).sptf)

print(mixed2)
print(mixed3)


#Answer to the 2nd question of part 3 (switch 75-25 bonds vs stocks):
# As expecting, the returns' trend of the mixed investor is  closer to the one of the agressive
# when its chances to invest in stocks are higher than its chances to invest in bonds (75-25 stocks vs bonds).
#However, when its chances to invest in bonds amount to 75, the tendency of the returns for the mixed investor
# gets closer to the one of the defensive.Compare to the 75-25 stocks vs bonds situations, the mixed investor clearly
# takes less risks, which is observable through a decrease of the variance of its returns over time
# when we pass from 75-25 stocks vs bonds to 25-75 stocks vs bonds.



#Multiply all investors' starting budget by 10

from Part3InvestorsClassDescription import Investor, Defensive, Agressive, Mixed
import pandas as pd


#Objects' creation for the sublass "Defensive investor"
#Simulation of 200 defensive investors

deff10=[]
for i in range(200):
    deff10.append(Defensive(10000*10).ptf)

print(deff10)

#Conversion of the list "deff10" to a dataframe in order to convert it into a CSV

deff10=pd.DataFrame(deff10)
deff10.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/deff10.csv')

#Objects' creation for the subclass "Agressive investor"
#Simulation of 200 agressive investors

agg10=[]
for i in range(200):
    agg10.append(Agressive(10000*10).ptf)

print(agg10)

#Conversion of the list "agg10" to a dataframe in order to convert it into a CSV

agg10=pd.DataFrame(agg10)
agg10.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/agg10.csv')

#Objects' creation for the sublass "Mixed investor"
#Simulation of 200 mixed investors

mixed=[]
mixed1 =[]
for i in range(200):
    mixed.append(Mixed(10000*10).bptf)
    mixed1.append(Mixed(10000*10).sptf)

print(mixed)
print(mixed1)

#Answer to the 3rd question of part 3 (multiplication of the budget by 10):
# When we multiply the starting budget of the investors by 10 ("agg10" file for agressive investors and "def10" file
# for defensive investors (see Result folder)), we observe that each investor, no matter the asset on which
#he invests (bonds or stocks) has a number a total number of investments which is much larger than
# when its budget was 1000 ("agg" file for agressive investors and "def" file for defensive investors (see Result folder)).
# Hence,there is, as it could have been expected, a positive correlation between the budget and the investment' frequency.