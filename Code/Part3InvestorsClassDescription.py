import pandas as pd
import numpy as np

ShortBonds=pd.read_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/ShortBonds.csv')
LongBonds=pd.read_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/LongBonds.csv')
stockk=pd.read_excel('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/stockk.xls')

# export data
stockk.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stockk.csv')


#Creation of the subclass "Defensive investor"

def bondportfolio(budge):
            data = pd.DataFrame({'ShortBonds': [0], 'LongBonds': [0]})
            while budge > ShortBonds['amount'].min():
                    short = np.random.choice(ShortBonds['amount'])
                    long = np.random.choice(LongBonds['amount'])
                    data = data.append({'ShortBonds': short, 'LongBonds': long }, ignore_index=True)
                    budge= budge - short -long
            return data

class Investor(object):
    def __init__(self, budget):   #each investor has a budget, which is the only attribute for the principal class "Investor"
        self.budget=budget



class Defensive(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.ptf=bondportfolio(budget)





#Creation of the subclass "Agressive investor"


def stockportfolio(budg):
    data = pd.DataFrame({'stocks': [0]})
    while budg > 100:
        a = np.random.choice(stockk['High'])
        data = data.append({'stocks': a}, ignore_index=True)
        budg = budg - a
    return data


class Agressive(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.ptf=stockportfolio(budget)





#Creation of the subclass "Mixed investor"

class      Mixed(Investor):
    def __init__(self, budget):
        self.abudget = budget
        self.bptf=bondportfolio(budget*0.25)
        self.sptf=stockportfolio(budget*0.75)



