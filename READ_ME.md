Honorine Lecat

Bizimana Luccin

M2 EEE 
2021/2022

# **Python project: Readme file**
\\
## 1) Project's brief description


In this project, we are seeking to model and graphically represent the behaviour of three different types
of investors: Agressive, mixed and defensive. These three kind of investors differentiate
between them by the financial product on which they invest in (stocks, short and long term bonds) as well the risks they take, with regard to their budget, when investing.

We are first going to set the features that define each financial product, such as the yearly interest rate or the minimum amount to be invested. Then, after having represented through Object Oriented Programming (OOP) and functionnal
the way the different individuals behave in their investment choice, we will simulate some investment's scenario in order to identify what year or what type of investment  was the most profitable for each type of investor.

\\
## 2) Files

### 2.1) Data and Results


####2.1.1) Creation of dataframes:

When we had first to model the two types of bounds, namely the short term bound and the long term bound,
we have generated, for each kind of bound, a dataset with 5 variables (the acquisition date, the amount invested on the bounds, the bound's minimum price, the yearly interest rate and the minimum term) which represented the way the 2 bounds were defined.
Once the 2 datasets (named **ShortBounds** and **LongBounds**) generated, we convert them into CSV files using the following codeline:


```

ShortBonds.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/ShortBonds.csv')
LongBonds.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/LongBonds.csv')

```

After having built the short and long terms bonds' datasets, we were able to plot the evolution of
the minimum amount to be invested in each bound over a period of 50 years. The result was the following:

![Evolution_bonds](C:/Users/Bizimana/group1_tse_exam_2021-22/Result/Evolution_bondsINV.png)

\\


\\


####2.1.2) Extraction of the stocks' price from *Yahoo Finance*:
```

tickers = ['FDX','GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']
stock = web.DataReader(tickers, data_source = 'yahoo', start = '2016-01-09', end = '2021-01-01')

stock.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stock.csv')

```
To carry out this project, we relied on a CSV file containing stock's price, from a period of 09/01/2016 to 01/01/2021, of 7 well-known companies: Googl (Alphabet), FDX (FedEx), IBM (International Business Machines), K0 (Coca Cola), MS (Morgan Stanley), NOK (Nokia) and XOM (ExxonMobil).
To obtain this CSV file, that we named "**stock**", we have extracted from *Yahoo Finance* the price of the stock for the period of interest for each of the stock just mentionned

This file was mostly used in the second part of our project when we modelled, for each stock, the evolution of both its price and
the return on investment it provided in the period from 09/01/2016 to 01/01/2021. The results were the following:


![StockPriceEvolution](C:/Users/Bizimana/group1_tse_exam_2021-22/Result/stock_price.png)
![StockReturnOnINVevolution](C:/Users/Bizimana/group1_tse_exam_2021-22/Result/daily_return.png)



####2.1.3) Conversion of investors simulations' lists into CSV files

For ease of use, in the parts 3 and 4, we convert the lists corresponding to the agressive and defensive investors' simulations, 
into CSV files. This help us to observe more easily what changes when investor' budget 
is multiply by 10 and so to answer the last question of part 4.


This is how we perform the conversion:

```
deff=pd.DataFrame(deff)
deff.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/deff.csv')

agg=pd.DataFrame(agg)
agg.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/agg.csv')

deff10=pd.DataFrame(deff10)
deff10.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/deff10.csv')

agg=pd.DataFrame(agg)
agg.to_csv('C:/Users/Bizimana/group1_tse_exam_2021-22/Data/agg.csv')

```





### 2.2) Codes and Libraries used

The python packages that greatly help us in the realization of this project are the ones commonly used when using mathematics to handle and manipulate table-structured data:
pandas and numpy. Moreover, we also relied on the function **pyplot**, that we imported from the data visualization package **matplotlib**. For example, in the first and second parts of our project (files "Short and Long term bonds" and "Stocks") the pandas and numpy packages were critical to build the datasets of the short and long term bonds while pyplot allows us to
visualize the graphs of the stocks' price and return on investment evolution from 2016 to 2021 and the graph representing the evolution over a 50 years' period of the minimum amount invested for the two types of bounds.

```
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


```
\\

Moreover, to extract from Yahoo Finance the stock's price of the seven well-known companies mentionned in the 2.1.2 part, 
we had first to import the library **pandas_datareader**. This Python's package is used when one wants to load on its working environment data comming from an external internet source in order to build dataframes. 
As this is exactly what we wanted to do, we used the pandas_datareader package.

```
import pandas_datareader as web
```


Besides, in the file "Investors", for the third part of our project, we relied on the package named **random** to be able to 
well represent the investment choices of the agressive, the mixed and the defensive investors. Indeed, each of them invests randomly on 
the different financial product (stock, short term bound, long term bound) and also chooses randomly the amount of stocks or bounds he buys gien its budget.

For these reasons, we had to use the functions **randint** and **random.choice()**, coming both from the library **random**.

```

import random

```


### 2.3) Objects Oriented Programming (Part 3 and Part 4)

In the two last parts of our project, we perform Objects Oriented Programming (OOP).
To bring structure and clarity to our codes we separate the class description part (file named "*Part3InvestorsClassDescription*" ) and the objects' creation (file named "*Part3InvestorsObjectsCreation*").
In the class Description file, we basically create a principal class named "Investor" whose only attribute called "budget" is inherited
by all the subclasses of Investor. The different subclasses from Investor correspond to the investors' types: agressive, defensive and mixed.

Something important is that all subclasses encompass a function named *portfolio* which model the investments' choices for each investor's type.

Example for the defensive investor:

```
def boundportfolio(budge):
            data = pd.DataFrame({'ShortBounds': [0], 'LongBounds': [0]})
            while budge > ShortBounds['amount'].min():
                    short = np.random.choice(ShortBounds['amount'])
                    long = np.random.choice(LongBounds['amount'])
                    data = data.append({'ShortBounds': short, 'LongBounds': long }, ignore_index=True)
                    budge= budge - short -long
            return data

```
